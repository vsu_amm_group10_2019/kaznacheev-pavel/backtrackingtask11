﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graf
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonCountTown_Click(object sender, EventArgs e)
        {
            int countTown = (int)this.numericUpDownCountTown.Value;
            dataGridViewTown.RowCount = countTown;
            dataGridViewTown.ColumnCount = countTown;
            for (int i=0; i < countTown; i++)
            {
                dataGridViewTown.Rows[i].HeaderCell.Value = i.ToString();
                dataGridViewTown.Columns[i].HeaderText = i.ToString();
            }
            
        }

        private void buttonDoTask_Click(object sender, EventArgs e)
        {
            int[,] matr = new int[dataGridViewTown.RowCount, dataGridViewTown.RowCount];
            for (int i = 0; i < dataGridViewTown.RowCount; i++)
            {
                for (int j = i + 1; j < dataGridViewTown.RowCount; j++)
                {
                    if (dataGridViewTown.Rows[i].Cells[j].Value != null)
                    {
                        matr[i, j] = Convert.ToInt32(dataGridViewTown.Rows[i].Cells[j].Value);
                        matr[j, i] = Convert.ToInt32(dataGridViewTown.Rows[i].Cells[j].Value);
                    }
                    else
                    {
                        matr[i, j] = 0;
                        matr[j, i] = 0;
                    }
                }
            }
            listBoxFromTown.Items.Clear();
            int N = (int)numericUpDownDistance.Value;
            Solver solver = new Solver(dataGridViewTown.RowCount, matr);
            if (solver.Solve((int)numericUpDownTownTo.Value, N))
            {
                List<Road> roads = solver.Solution;
                foreach (Road road in roads)
                {
                    listBoxFromTown.Items.Add($"Город {road.From}, расстояние {road.Distance}");
                }
                //MessageBox.Show("Можно");
            }
            else
            {
                MessageBox.Show($"Нет ни одной дороги соединяющей город {numericUpDownTownTo.Value} с другими городами");
            }
            /*Solver solver = new Solver(dataGridViewTown.RowCount, matr);
            if (solver.HasRoad((int)TownFrom.Value, (int)numericUpDownTownTo.Value))
            {
                MessageBox.Show("Можно");
            }
            else
            {
                MessageBox.Show("Нельзя");
            }*/
        }
    }
}
