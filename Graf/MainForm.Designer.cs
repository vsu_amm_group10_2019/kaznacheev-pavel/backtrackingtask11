﻿
namespace Graf
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewTown = new System.Windows.Forms.DataGridView();
            this.numericUpDownCountTown = new System.Windows.Forms.NumericUpDown();
            this.buttonCountTown = new System.Windows.Forms.Button();
            this.numericUpDownTownTo = new System.Windows.Forms.NumericUpDown();
            this.labelCountTown = new System.Windows.Forms.Label();
            this.labelTownTo = new System.Windows.Forms.Label();
            this.buttonDoTask = new System.Windows.Forms.Button();
            this.numericUpDownDistance = new System.Windows.Forms.NumericUpDown();
            this.labelDistance = new System.Windows.Forms.Label();
            this.listBoxFromTown = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCountTown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTownTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDistance)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewTown
            // 
            this.dataGridViewTown.AllowUserToAddRows = false;
            this.dataGridViewTown.AllowUserToDeleteRows = false;
            this.dataGridViewTown.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridViewTown.Location = new System.Drawing.Point(25, 12);
            this.dataGridViewTown.Name = "dataGridViewTown";
            this.dataGridViewTown.Size = new System.Drawing.Size(658, 324);
            this.dataGridViewTown.TabIndex = 0;
            // 
            // numericUpDownCountTown
            // 
            this.numericUpDownCountTown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownCountTown.Location = new System.Drawing.Point(39, 373);
            this.numericUpDownCountTown.Name = "numericUpDownCountTown";
            this.numericUpDownCountTown.Size = new System.Drawing.Size(75, 20);
            this.numericUpDownCountTown.TabIndex = 1;
            // 
            // buttonCountTown
            // 
            this.buttonCountTown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonCountTown.Location = new System.Drawing.Point(39, 410);
            this.buttonCountTown.Name = "buttonCountTown";
            this.buttonCountTown.Size = new System.Drawing.Size(75, 23);
            this.buttonCountTown.TabIndex = 2;
            this.buttonCountTown.Text = "Ввести";
            this.buttonCountTown.UseVisualStyleBackColor = true;
            this.buttonCountTown.Click += new System.EventHandler(this.buttonCountTown_Click);
            // 
            // numericUpDownTownTo
            // 
            this.numericUpDownTownTo.Location = new System.Drawing.Point(496, 373);
            this.numericUpDownTownTo.Name = "numericUpDownTownTo";
            this.numericUpDownTownTo.Size = new System.Drawing.Size(75, 20);
            this.numericUpDownTownTo.TabIndex = 3;
            // 
            // labelCountTown
            // 
            this.labelCountTown.AutoSize = true;
            this.labelCountTown.Location = new System.Drawing.Point(22, 346);
            this.labelCountTown.Name = "labelCountTown";
            this.labelCountTown.Size = new System.Drawing.Size(110, 13);
            this.labelCountTown.TabIndex = 4;
            this.labelCountTown.Text = "Количество городов";
            // 
            // labelTownTo
            // 
            this.labelTownTo.AutoSize = true;
            this.labelTownTo.Location = new System.Drawing.Point(493, 346);
            this.labelTownTo.Name = "labelTownTo";
            this.labelTownTo.Size = new System.Drawing.Size(98, 13);
            this.labelTownTo.TabIndex = 5;
            this.labelTownTo.Text = "Выбранный город";
            // 
            // buttonDoTask
            // 
            this.buttonDoTask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDoTask.Location = new System.Drawing.Point(496, 410);
            this.buttonDoTask.Name = "buttonDoTask";
            this.buttonDoTask.Size = new System.Drawing.Size(188, 23);
            this.buttonDoTask.TabIndex = 6;
            this.buttonDoTask.Text = "Решить";
            this.buttonDoTask.UseVisualStyleBackColor = true;
            this.buttonDoTask.Click += new System.EventHandler(this.buttonDoTask_Click);
            // 
            // numericUpDownDistance
            // 
            this.numericUpDownDistance.Location = new System.Drawing.Point(609, 373);
            this.numericUpDownDistance.Name = "numericUpDownDistance";
            this.numericUpDownDistance.Size = new System.Drawing.Size(75, 20);
            this.numericUpDownDistance.TabIndex = 7;
            // 
            // labelDistance
            // 
            this.labelDistance.AutoSize = true;
            this.labelDistance.Location = new System.Drawing.Point(617, 346);
            this.labelDistance.Name = "labelDistance";
            this.labelDistance.Size = new System.Drawing.Size(67, 13);
            this.labelDistance.TabIndex = 8;
            this.labelDistance.Text = "Расстояние";
            // 
            // listBoxFromTown
            // 
            this.listBoxFromTown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBoxFromTown.FormattingEnabled = true;
            this.listBoxFromTown.ItemHeight = 20;
            this.listBoxFromTown.Location = new System.Drawing.Point(701, 12);
            this.listBoxFromTown.Name = "listBoxFromTown";
            this.listBoxFromTown.Size = new System.Drawing.Size(290, 324);
            this.listBoxFromTown.TabIndex = 10;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 468);
            this.Controls.Add(this.listBoxFromTown);
            this.Controls.Add(this.labelDistance);
            this.Controls.Add(this.numericUpDownDistance);
            this.Controls.Add(this.buttonDoTask);
            this.Controls.Add(this.labelTownTo);
            this.Controls.Add(this.labelCountTown);
            this.Controls.Add(this.numericUpDownTownTo);
            this.Controls.Add(this.buttonCountTown);
            this.Controls.Add(this.numericUpDownCountTown);
            this.Controls.Add(this.dataGridViewTown);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Name = "MainForm";
            this.Text = "MainForm";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCountTown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTownTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDistance)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewTown;
        private System.Windows.Forms.NumericUpDown numericUpDownCountTown;
        private System.Windows.Forms.Button buttonCountTown;
        private System.Windows.Forms.NumericUpDown numericUpDownTownTo;
        private System.Windows.Forms.Label labelCountTown;
        private System.Windows.Forms.Label labelTownTo;
        private System.Windows.Forms.Button buttonDoTask;
        private System.Windows.Forms.NumericUpDown numericUpDownDistance;
        private System.Windows.Forms.Label labelDistance;
        private System.Windows.Forms.ListBox listBoxFromTown;
    }
}

