﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graf
{
    public class Solver
    {
        public int[,] Matr { get; set; }
        private int Number { get; set; }
        public List<Road> Solution { get; set; } = new List<Road>();
        public Solver(int number, int[,] matr)
        {
            Number = number;
            Matr = matr;
        }
        // Решение задачи
        public bool Solve(int second, int distance)
        {
            int N = distance;
            for (int roadFrom = 0; roadFrom < Number; roadFrom++)
            {
                if (HasRoad(roadFrom, second) > N && roadFrom != second)
                {
                    Road road = new Road { From = roadFrom, Distance = HasRoad(roadFrom, second) };
                    Solution.Add(road);
                }
            }
            if (Solution.Count == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private int HasRoad(int first, int second)
        {
            HashSet<int> visited = new HashSet<int>();
            return HasRoad(first, second, visited);
        }
        // Проверка на существование дороги между first и second
        // Возвращает расстояние между first и second
        private int HasRoad(int first, int second, HashSet<int> visited)
        {
            int distanceBtwTown = 0;
            if (Matr[first, second] != 0)
            {
                distanceBtwTown = Matr[first, second];
                return distanceBtwTown;
            }
            else
            {
                visited.Add(first);
                for (int i = 0; i < Number; i++)
                {
                    if (Matr[first, i] != 0 && !visited.Contains(i))
                    {
                        distanceBtwTown = distanceBtwTown + Matr[first, i];
                        if (HasRoad(i, second, visited) != 0)
                        {
                            return distanceBtwTown + Matr[i, second];
                        }
                    }
                }
                return 0;
            }
        }
    }
}
